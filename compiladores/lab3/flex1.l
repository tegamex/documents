%{

#include<math.h>
#define NUM	300
#define REAL	310
#define ID	320
#define IF	330

%}

%option noyywrap

DIGITO [0-9]

%%

{DIGITO}+	return NUM;

%%

int main(int argc,char **argv){
	yylex();
	return 0;
}
