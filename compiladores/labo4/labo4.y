%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
char lexema[60];
void yyerror(char *msg);
typedef struct{char nombre[60];double valor;int token;}tipoTS;
tipoTS TablaSim[100];
int nSim = 0;
typedef struct{int op;int a1;int a2;int a3;}tipoCodigo;
int cx = -1;
tipoCodigo TCodigo[100];
void generaCodigo(int,int,int,int);
int localizaSimb(char *, int);
void imprimeTablaSim();
int nVarTemp = 0;
int GenVarTemp();
%}

%token PROGRAMA ID INICIO FIN NUM VARIABLE ASIGNAR SUMAR RESTAR MULTIPLICAR DIVIDIR POTENCIA DESIGUAL

%%

S: PROGRAMA ID ';' INICIO listaInstr FIN '.';
listaInstr: instr listaInstr | ;
instr: ID{$$=localizaSimb(lexema,ID);} '=' expr{generaCodigo(ASIGNAR,$2,$4,'-');}';';
expr: expr SUMAR term{int i = GenVarTemp(); generaCodigo(SUMAR,i,$1,$3); $$=i;};
expr: expr RESTAR term{int i = GenVarTemp(); generaCodigo(RESTAR,i,$1,$3); $$=i;};
expr: term ;
term: term MULTIPLICAR fact{int i = GenVarTemp(); generaCodigo(MULTIPLICAR,i,$1,$3); $$=i;};
term: term DIVIDIR fact{int i = GenVarTemp(); generaCodigo(DIVIDIR,i,$1,$3); $$=i;};
term: fact;
fact: aux;
fact: aux '^' aux {int i = GenVarTemp(); generaCodigo(POTENCIA,i,$1,$3); $$=i;};
fact: aux '<' '>' aux {int i = GenVarTemp(); generaCodigo(DESIGUAL,i,$1,$4); $$=i;};
aux:NUM{$$=localizaSimb(lexema,NUM);};
aux:ID{$$=localizaSimb(lexema,ID);};
aux:'(' expr ')' { $$=$2;};

%%
int GenVarTemp(){
  char t[60];
  sprintf(t,"_T%d",nVarTemp++);
  return localizaSimb(t,ID);
}

void generaCodigo(int op, int a1, int a2, int a3){
  cx++;
  TCodigo[cx].op = op;
  TCodigo[cx].a1 = a1;
  TCodigo[cx].a2 = a2;
  TCodigo[cx].a3 = a3;
}

int localizaSimb(char *nom, int tok){
  int i;
  for(i=0; i<nSim; i++){
    if(!strcasecmp(TablaSim[i].nombre,nom))
      return i;
  }
  strcpy(TablaSim[nSim].nombre,nom);
  TablaSim[nSim].token = tok;
  if(tok==ID) TablaSim[nSim].valor = 0.0;
  if(tok==NUM) sscanf(nom,"%lf",&TablaSim[nSim].valor);
  nSim++;
  return nSim - 1;
}

void imprimeTablaSim(){
  int i;
  for(i=0; i<nSim; i++){
    printf("%d nombre = %s tok = %d valor = %lf\n",i,TablaSim[i].nombre,TablaSim[i].token,TablaSim[i].valor);
  }
}

void imprimeTablaCod(){
  int i;
  for(i=0; i<=cx; i++){
    printf("%d a1 = %d a2 = %d a3 = %d\n",TCodigo[i].op,TCodigo[i].a1,TCodigo[i].a2,TCodigo[i].a3);
  }
}

void InterpretaCodigo(){
  int i,a1,a2,a3,op;
  for(i=0; i<=cx; i++){
    op = TCodigo[i].op;
    a1 = TCodigo[i].a1;
    a2 = TCodigo[i].a2;
    a3 = TCodigo[i].a3;
    if(op==ASIGNAR) TablaSim[a1].valor = TablaSim[a2].valor;
    if(op==SUMAR) TablaSim[a1].valor = TablaSim[a2].valor + TablaSim[a3].valor;
    if(op==RESTAR) TablaSim[a1].valor = TablaSim[a2].valor - TablaSim[a3].valor;
    if(op==MULTIPLICAR) TablaSim[a1].valor = TablaSim[a2].valor * TablaSim[a3].valor;
    if(op==DIVIDIR) TablaSim[a1].valor = TablaSim[a2].valor / TablaSim[a3].valor;
    if(op==POTENCIA) TablaSim[a1].valor = pow(TablaSim[a2].valor,TablaSim[a3].valor);
    if(op==DESIGUAL) TablaSim[a1].valor = TablaSim[a2].valor == TablaSim[a3].valor ? 0 : 1;
  }
}

void yyerror(char *msg){
  printf("ERROR: %s\n",msg);
}

int EsPalabraReservada(char lexema[]){
  //strcmp considera mayusculas y minusculas
  //strcasecmp ignora mayusculas de minusculas
  if(strcasecmp(lexema,"Program")==0) return PROGRAMA;
  if(strcasecmp(lexema,"Begin")==0) return INICIO;
  if(strcasecmp(lexema,"End")==0) return FIN;
  if(strcasecmp(lexema,"Var")==0) return VARIABLE;
  return ID;
}

int yylex(){
  char c; int i;
  while(1){
    c = getchar();
    if(c==' ') continue;
    if(c=='\t') continue;
    if(c=='\n') continue;
    if(c=='+') return SUMAR;
    if(c=='-') return RESTAR;
    if(c=='*') return MULTIPLICAR;
    if(c=='/') return DIVIDIR;
    if(isdigit(c)){
      i = 0;
      do{
        lexema[i++] = c;
        c = getchar();
      }while(isdigit(c));
      ungetc(c,stdin);
      lexema[i] = '\0';
      //lexema[i] = 0;
      return NUM;
    }
    if(isalpha(c)){
      i = 0;
      do{
        lexema[i++] = c;
        c = getchar();
      }while(isalnum(c));
      ungetc(c,stdin);
      lexema[i] = '\0';
      //lexema[i] = 0;
      return EsPalabraReservada(lexema);
    }
    return c;
  }
}

int main(){
  if(!yyparse()){
	  printf("La cadena es valida\n");
	  printf("Tabla de Simbolos:\n"); imprimeTablaSim();
	  printf("Tabla de Codigos\n"); imprimeTablaCod();
	  InterpretaCodigo();
	  printf("Nueva tabla:\n"); imprimeTablaSim();
	}
  else printf("La cadena es INVALIDA\n");
}
