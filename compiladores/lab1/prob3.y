%{
#include<stdio.h>
#include<string.h>
char lexema[255];
void yyerror(char *);
%}

%token ID IGUAL NUM PUNTCOM OP LETRA PUNT DIG

%%		
instr: asig sum;
asig: asig ID IGUAL numero PUNTCOM;
asig: ;
numero: NUM;
numero: DIG PUNT exp LETRA NUM;
exp: NUM;
exp: DIG;
exp: '0';
sum: sum ID IGUAL ID OP ID PUNTCOM;
sum: ;

%%
void yyerror(char *mgs){
	printf("error:%s",mgs);
}
int yylex(){
	char c;
	while(1){
		c=getchar();
		if(c=='\n') continue;
		if(isalpha(c)){ //verifica si es un caracter
			int i=0;
			do{
				lexema[i++]=c;
				c=getchar();
				}while(isalnum(c));
			ungetc(c,stdin); //devuelve el caracter a la entrada estandar
			lexema[i]=0;
			return ID; //devuelve el token
		}
		if(c==';') return PUNTCOM;
		if(c=='.') return PUNT;
		if(c=='=') return IGUAL;
		if(isdigit(c)){ //verifica si es un nro entero
			int i=0;
			do{
				lexema[i++]=c;
				c=getchar();
				}while(isdigit(c));
			ungetc(c,stdin); //devuelve el caracter a la entrada estandar
			lexema[i]=0;
			if(i==1){
			if(c!='0') return DIG;
			}
			else return NUM; //devuelve el token
		}
		if(c=='+'||c=='-'||c=='*'||c=='/') return OP;
		return c;
	}
}
int main(){
	if(!yyparse()) printf("cadena valida\n");
	else printf("cadena invalida\n");
	return 0;
}
