%{
#include<stdio.h>
#include<string.h>
char lexema[255];
void yyerror(char *);
%}

%token NUM PUNT

%%		
instr: instr NUM PUNT NUM;
instr: instr NUM;
instr: ;

%%
void yyerror(char *mgs){
	printf("error:%s",mgs);
}
int yylex(){
	char c;
	while(1){
		c=getchar();
		if(c=='\n') continue;
		if(c=='.') return PUNT;
		if(isdigit(c)){ //verifica si es un nro entero
			int i=0;
			do{
				lexema[i++]=c;
				c=getchar();
				}while(isdigit(c));
			ungetc(c,stdin); //devuelve si es nro entero
			lexema[i]=0;
			return NUM; //devuelve el token
		}
		return c;
	}
}
int main(){
	if(!yyparse()) printf("cadena valida\n");
	else printf("cadena invalida\n");
	return 0;
}
