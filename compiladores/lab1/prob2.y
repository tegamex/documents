%{
#include<stdio.h>
#include<string.h>
char lexema[255];
void yyerror(char *);
%}

%token DIG PUNT NUM LETRA

%%		
instr: instr DIG PUNT exp LETRA exp;
instr: ;
exp: NUM;
exp: DIG;
exp: '0';

%%
void yyerror(char *mgs){
	printf("error:%s",mgs);
}
int yylex(){
	char c;
	while(1){
		c=getchar();
		if(c=='\n') continue;
		if(c=='E') return LETRA;
		if(c=='.') return PUNT;
		if(isdigit(c)){ //verifica si es un nro entero
			int i=0;
			do{
				lexema[i++]=c;
				c=getchar();
				}while(isdigit(c));
			ungetc(c,stdin); //devuelve el caracter a la entrada estandar
			lexema[i]=0;
			if(i==1){
			if(lexema[0]!='0') return DIG;
			}
			else return NUM; //devuelve el token
		}
		return c;
	}
}
int main(){
	if(!yyparse()) printf("cadena valida\n");
	else printf("cadena invalida\n");
	return 0;
}
