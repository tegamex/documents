%{
#include<stdio.h>
#include<string.h>
char lexema[255];
void yyerror(char *);
%}

%token MAS MENOS NUM

%%		
expr:expr MAS term|term|expr MENOS term;
term: term '*' fact|term '/' fact | fact;
fact: NUM;

%%
void yyerror(char *mgs){
	printf("error:%s",mgs);
}
int yylex(){
	char c;
	while(1){
		c=getchar();
		if(c=='\n') continue;
		if(isdigit(c)){ //verifica si es un nro entero
			int i=0;
			do{
				lexema[i++]=c;
				c=getchar();
				}while(isdigit(c));
			ungetc(c,stdin); //devuelve el caracter a la entrada estandar
			return NUM; //devuelve el token
		}
		if(c=='+') return MAS;
        if(c=='-') return MENOS;
		return c;
	}
}
int main(){
	if(!yyparse()) printf("cadena valida\n");
	else printf("cadena invalida\n");
	return 0;
}
