%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
char lexema[60];
char cadenaaux[60];
int posicion[100],pos=0;

typedef struct{
	char nombre[60];
	double valor;
	int token;
} tipoTS;
tipoTS TablaSim[100];
int nSim = 0;

typedef struct{
	int op;
	int a1;
	int a2;
	int a3;
} tipoCodigo;
tipoCodigo TCodigo[100];
int cx = -1;

typedef struct{
    char nombre[50];
    int index;
	 int par;
	 int pos;
} funcion;
funcion funciones[100];
int ind=0;

char constantes[100][20];
int cons=0;
int condicional[100];
int cond=0;

char funcionactual[50];
int parametros=0;

int m;
void yyerror(char *msg);
void generaCodigo(int,int,int,int);
int localizaSimb(char *, int);
void imprimeTablaSim();
int GenVarTemp();
int nVarTemp = 0;
void InterpretaCodigo();
void imprimeTablaCod();
void ingresafuncion(char *,int);
void ingresaconstante(char *);
%}

%token PROGRAMA FUNCTION ID INICIO FIN NUM VARIABLE SI ASIGNAR SUMAR RESTAR MULTIPLICAR DIVIDIR POTENCIA DESIGUAL ENTONCES 
	MAYOR MENOR IGUAL MAYORIGUAL MENORIGUAL SALTA SALTAv2 SALTANO MIENTRAS IMPRIMIR REGRESA PARA NOMBREFUNCION 
	VOYFUNCION FINFUNCION CONST CONSTANTE NEG
%nonassoc IFX
%nonassoc SINO
%%

S: PROGRAMA ID ';' INICIO funciones FIN;
funciones: funcion funciones | ;
funcion: FUNCTION ID{ ingresafuncion(lexema,cx); strcpy(funcionactual,lexema); funciones[ind-1].pos=nSim; parametros=0;} 
			'(' parametros{ funciones[ind-1].par=parametros; } ')' '<' 
			listaInstr '>'{ generaCodigo(FINFUNCION,'-','-','-');} ;
parametros: | ID{ strcpy(cadenaaux,funcionactual); strcat(cadenaaux,"."); strcat(cadenaaux,lexema); $$=localizaSimb(cadenaaux,ID);
				  		parametros++; } otrosparametros ;
otrosparametros: ',' ID{ strcpy(cadenaaux,funcionactual); strcat(cadenaaux,"."); strcat(cadenaaux,lexema); $$=localizaSimb(cadenaaux,ID); parametros++;} otrosparametros | ;
listaInstr: instr listaInstr | ;
instr: IMPRIMIR expr ';' { generaCodigo(IMPRIMIR,$2,'-','-');} ;
//instr: LEER ID{ $$=localizaSimb(lexema,ID); generaCodigo(LEER,$$,'-','-');} ';';

instr: NOMBREFUNCION{ m=encuentrafuncion(lexema); parametros=0; } 
		'(' valores{ if(parametros!=funciones[m].par){printf("error*\n");return -1;} generaCodigo(VOYFUNCION,funciones[m].index,'-','-'); } ')' ';';
valores: | expr{generaCodigo(ASIGNAR,funciones[m].pos,$1,'-'); parametros++;} otrosvalores;
otrosvalores:',' expr{generaCodigo(ASIGNAR,funciones[m].pos+parametros,$2,'-'); parametros++;} otrosvalores | ;

instr: MIENTRAS{ $1=cx;} cond{ generaCodigo(SALTA,$3,'-','-');$3=cx;} ':' 
        bloque{generaCodigo(REGRESA,$1,'-','-'); TCodigo[$3].a2=cx;};
instr: PARA '[' asignar{ $3=cx; } '#' 
					
					 cond{ generaCodigo(SALTAv2,$6,'-','-'); $6 =cx; } '#'

					 asignar{ generaCodigo(REGRESA,$3,'-','-'); TCodigo[$6].a2=cx;}

			    ']' 
					 bloque{ generaCodigo(REGRESA,$6,'-','-'); TCodigo[$6].a3=cx; } ;
asignar: asignacion | ;
asignacion: valor{strcpy(cadenaaux,funcionactual); strcat(cadenaaux,".");strcat(cadenaaux,lexema); $$=localizaSimb(cadenaaux,ID);} 
				'=' expr{generaCodigo(ASIGNAR,$2,$4,'-'); };
valor: ID | VARIABLE;
bloque: instr | INICIO listaInstr FIN;

instr: CONST asignacion { ingresaconstante(TablaSim[$$].nombre); }';' ;
instr: asignacion ';';
instr: afirmar negar{cond--;};
afirmar: SI cond{ generaCodigo(SALTA,$2,'-','-'); condicional[cond++]=$2; $2=cx; } ':'
			bloque { TCodigo[$2].a2=cx; } 
negar : %prec IFX|SINO{ generaCodigo(SALTANO,condicional[cond-1],'-','-'); $1=cx;} 
						bloque{ TCodigo[$1].a2=cx; };

cond: expr '>' expr{int i = GenVarTemp(); generaCodigo(MAYOR,i,$1,$3); $$=i;};
cond: expr '<' expr{int i = GenVarTemp(); generaCodigo(MENOR,i,$1,$3); $$=i;};
cond: expr '=' expr{int i = GenVarTemp(); generaCodigo(IGUAL,i,$1,$3); $$=i;};
cond: expr '>''=' expr{int i = GenVarTemp(); generaCodigo(MAYORIGUAL,i,$1,$4); $$=i;};
cond: expr '<''=' expr{int i = GenVarTemp(); generaCodigo(MENORIGUAL,i,$1,$4); $$=i;};

expr: expr SUMAR term{int i = GenVarTemp(); generaCodigo(SUMAR,i,$1,$3); $$=i;};
expr: expr RESTAR term{int i = GenVarTemp(); generaCodigo(RESTAR,i,$1,$3); $$=i;};
expr: term ;
term: term MULTIPLICAR fact{int i = GenVarTemp(); generaCodigo(MULTIPLICAR,i,$1,$3); $$=i;};
term: term DIVIDIR fact{int i = GenVarTemp(); generaCodigo(DIVIDIR,i,$1,$3); $$=i;};
term: fact;
fact: aux;
fact: aux POTENCIA aux {int i = GenVarTemp(); generaCodigo(POTENCIA,i,$1,$3); $$=i;};
aux:NUM{$$=localizaSimb(lexema,NUM);};
aux:VARIABLE{strcpy(cadenaaux,funcionactual);strcat(cadenaaux,".");strcat(cadenaaux,lexema);$$=localizaSimb(cadenaaux,ID);};
aux:CONSTANTE{strcpy(cadenaaux,funcionactual);strcat(cadenaaux,".");strcat(cadenaaux,lexema);$$=localizaSimb(cadenaaux,ID);};
aux:NUM{strcpy(cadenaaux,lexema);} '.' NUM { strcat(cadenaaux,"."); strcat(cadenaaux,lexema); $$=localizaSimb(cadenaaux,NUM); };
aux:'(' expr ')' { $$=$2;};

%%

void InterpretaCodigo(){
	int i,a1,a2,a3,op;
	for(i=funciones[encuentrafuncion("main")].index+1; i<=cx; i++){
		op = TCodigo[i].op;
		a1 = TCodigo[i].a1;
		a2 = TCodigo[i].a2;
		a3 = TCodigo[i].a3;
		printf("i = %d op = %d a1 = %d a2 = %d a3 = %d\n",i,op,a1,a2,a3);
		
		if(op==ASIGNAR) TablaSim[a1].valor = TablaSim[a2].valor;
		if(op==SUMAR) TablaSim[a1].valor = TablaSim[a2].valor + TablaSim[a3].valor;
		if(op==RESTAR) TablaSim[a1].valor = TablaSim[a2].valor - TablaSim[a3].valor;
		if(op==MULTIPLICAR) TablaSim[a1].valor = TablaSim[a2].valor * TablaSim[a3].valor;
		if(op==DIVIDIR) TablaSim[a1].valor = TablaSim[a2].valor / TablaSim[a3].valor;
		if(op==POTENCIA) TablaSim[a1].valor = pow(TablaSim[a2].valor,TablaSim[a3].valor);
		if(op==DESIGUAL) TablaSim[a1].valor = TablaSim[a2].valor == TablaSim[a3].valor ? 0 : 1;
		if(op==NEG) TablaSim[a1].valor=-TablaSim[a2].valor;
		if(op==SALTA) if(TablaSim[a1].valor == 0)  i = a2;
		if(op==SALTANO) if(TablaSim[a1].valor== 1) i=a2;
		if(op==SALTAv2) if(TablaSim[a1].valor == 1) i = a2; else i = a3;
		if(op==REGRESA) i=a1;
		if(op==IMPRIMIR) printf("%f\n",TablaSim[a1].valor);
		//if(op==LEER) scanf("%f",&(TablaSim[a1].valor));
		if(op==VOYFUNCION) { posicion[pos++]=i; i=a1; }
		if(op==FINFUNCION)
			if(pos!=0) i=posicion[--pos];
			else i=cx;
		if(op==MAYOR)
			if(TablaSim[a2].valor>TablaSim[a3].valor) TablaSim[a1].valor=1;
			else TablaSim[a1].valor=0;
		if(op==MENOR)
			if(TablaSim[a2].valor<TablaSim[a3].valor) TablaSim[a1].valor=1;
			else TablaSim[a1].valor=0;
		if(op==IGUAL)
			if(TablaSim[a2].valor==TablaSim[a3].valor) TablaSim[a1].valor=1;
			else TablaSim[a1].valor=0;
		if(op==MAYORIGUAL)
			if(TablaSim[a2].valor>=TablaSim[a3].valor) TablaSim[a1].valor=1;
			else TablaSim[a1].valor=0;
		if(op==MENORIGUAL)
			if(TablaSim[a2].valor<=TablaSim[a3].valor) TablaSim[a1].valor=1;
			else TablaSim[a1].valor=0;
	}
}

int GenVarTemp(){
	char t[60];
	sprintf(t,"_T%d",nVarTemp++);
	return localizaSimb(t,ID);
}

void generaCodigo(int op, int a1, int a2, int a3){
	cx++;
	TCodigo[cx].op = op;
	TCodigo[cx].a1 = a1;
	TCodigo[cx].a2 = a2;
	TCodigo[cx].a3 = a3;
}
void ingresafuncion(char *nombre,int index){
	strcpy(funciones[ind].nombre,nombre);
	funciones[ind++].index=index;
}
void ingresaconstante(char *nombre){
	strcpy(constantes[cons++],nombre);
}

int localizaSimb(char *nom, int tok){
	int i;
	for(i=0; i<nSim; i++){
		if(!strcasecmp(TablaSim[i].nombre,nom))
			return i;
	}
	strcpy(TablaSim[nSim].nombre,nom);
	TablaSim[nSim].token = tok;
	if(tok==ID) TablaSim[nSim].valor = 0.0;
	if(tok==NUM) sscanf(nom,"%lf",&TablaSim[nSim].valor);
	nSim++;
	return nSim - 1;
}
int encuentrafuncion(char *nombre){
	int i;
	for(i=0;i<ind;i++) if(strcasecmp(nombre,funciones[i].nombre)==0) return i;
	return 0;
}

void imprimeTablaSim(){
	int i;
	for(i=0; i<nSim; i++){
		printf("%d nombre = %s tok = %d valor = %lf\n",i,TablaSim[i].nombre,TablaSim[i].token,TablaSim[i].valor);
	}
}

void imprimeTablaCod(){
	int i;
	for(i=0; i<=cx; i++){
		printf("%d ",i);
		switch(TCodigo[i].op){
			case ASIGNAR:
				printf("ASIGNAR"); break;
			case SUMAR:
				printf("SUMAR"); break;
			case RESTAR:
				printf("RESTAR"); break;
			case MULTIPLICAR:
				printf("MULTIPLICAR"); break;
			case POTENCIA:
				printf("POTENCIA"); break;
			case DESIGUAL:
				printf("DESIGUAL"); break;
			case SALTA: 
				printf("SALTA"); break;
			case SALTANO:
				printf("SALTANO"); break;
			case REGRESA:
				printf("REGRESA"); break;
			case IMPRIMIR:
				printf("IMPRIMIR"); break;
			case VOYFUNCION:
				printf("VOYFUNCION"); break;
			case FINFUNCION:
				printf("FINFUNCION"); break;
			case MAYOR:
				printf("MAYOR"); break;
			case MENOR:
				printf("MENOR"); break;
			case IGUAL:
				printf("IGUAL"); break;
			case MAYORIGUAL:
				printf("MAYORIGUAL"); break;
			case MENORIGUAL:
				printf("MENORIGUAL"); break;
			case SALTAv2:
				printf("SALTAv2"); break;
			case NEG:
				printf("NEG"); break;
		}
		printf(" %d a1 = %d a2 = %d a3 = %d\n",TCodigo[i].op,TCodigo[i].a1,TCodigo[i].a2,TCodigo[i].a3);
	}
}

void yyerror(char *msg){
	printf("ERROR: %s\n",msg);
}

int EsPalabraReservada(char lexema[]){
	//strcmp considera mayusculas y minusculas
	//strcasecmp ignora mayusculas de minusculas
	int i;
	if(strcmp(lexema,"algoritmo")==0) return PROGRAMA;
	if(strcmp(lexema,"inicio")==0) return INICIO;
	if(strcmp(lexema,"fin")==0) return FIN;
	if(strcmp(lexema,"si")==0) return SI;
	if(strcmp(lexema,"sino")==0) return SINO;
	if(strcmp(lexema,"mientras")==0) return MIENTRAS;
	//if(strcmp(lexema,"hacer")==0) return HACER;
	//if(strcmp(lexema,"endif")==0) return TERMINO;
	if(strcmp(lexema,"imprimir")==0) return IMPRIMIR;
	if(strcmp(lexema,"para")==0) return PARA;
	if(strcmp(lexema,"funcion")==0) return FUNCTION;
	if(strcmp(lexema,"const")==0) return CONST;
	//if(strcmp(lexema,"read")==0) return LEER;
	for(i=0;i<ind;i++)	if(strcmp(lexema,funciones[i].nombre)==0) return NOMBREFUNCION;
	strcpy(cadenaaux,funcionactual);strcat(cadenaaux,".");strcat(cadenaaux,lexema);
	for(i=0;i<cons;i++)	if(strcmp(cadenaaux,constantes[i])==0) return CONSTANTE;
	for(i=0;i<nSim;i++)	if(strcmp(cadenaaux,TablaSim[i].nombre)==0) return VARIABLE;
	return ID;
}

int yylex(){
	char c; int i;
	while(1){
		c = getchar();
		if(c==' ') continue;
		if(c=='\t') continue;
		if(c=='\n') continue;
		if(c=='+') return SUMAR;
		if(c=='-') return RESTAR;
		if(c=='*') return MULTIPLICAR;
		if(c=='/') return DIVIDIR;
		if(c=='^') return POTENCIA;
		if(isdigit(c)){
			i = 0;
			do{
				lexema[i++] = c;
				c = getchar();
			}while(isdigit(c));
			ungetc(c,stdin);
			lexema[i] = '\0';
			//lexema[i] = 0;
			return NUM;
		}
		if(isalpha(c)){
			i = 0;
			do{
				lexema[i++] = c;
				c = getchar();
			}while(isalnum(c));
			ungetc(c,stdin);
			lexema[i] = '\0';
			//lexema[i] = 0;
			return EsPalabraReservada(lexema);
		}
		return c;
	}
}

int main(){
	if(!yyparse()){ 
		printf("La cadena es valida\n");
		//printf("Tabla de Simbolos:\n");imprimeTablaSim();
		printf("Tabla de Codigos\n"); imprimeTablaCod();
		//despues de interpreta codigo
		printf("Interpretaremos el codigo\n"); InterpretaCodigo();
		printf("Despues de interpreta codigo: tabla de simbolos\n");imprimeTablaSim();
	}
	else printf("La cadena es INVALIDA\n");
	return 0;
}
